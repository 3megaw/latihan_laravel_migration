@extends('layout.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Casts Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <a class="btn btn-primary" href='/casts/create'>Create New Cast</a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse($cast as $key => $cast)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$cast->nama}}</td>
                <td>{{$cast->umur}}</td>
                <td>{{$cast->bio}}</td>
                <td style="display: flex;">
                    <a href="/casts/{{$cast->id}}" class="btn btn-info btn-sm">show</a>
                    <a href="/casts/{{$cast->id}}/edit" class="btn btn-default btn-sm">edit</a>
                    <form action="/casts/{{$cast->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">No Casts</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
    {{-- <div class="card-footer clearfix">
      <ul class="pagination pagination-sm m-0 float-right">
        <li class="page-item"><a class="page-link" href="#">«</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">»</a></li>
      </ul>
    </div> --}}
  </div>
@endsection
