@extends('layout.master');

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/casts/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="inputnama">Nama</label>
          <input type="text" class="form-control" name="nama" value="{{old('nama',$cast->nama)}}" placeholder="Masukan nama">
          @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="inputumur">Umur</label>
          <input type="number" class="form-control" name="umur" value="{{old('umur',$cast->umur)}}" placeholder="Masukan umur">
          @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
          @enderror
        </div>
        <div class="form-group">
            <label for="inputbio">Bio</label>
            <input type="text" class="form-control" name="bio" value="{{old('bio',$cast->bio)}}" placeholder="Masukan umur">
            @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
          @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
@endsection
