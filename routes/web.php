<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/table', function () {
    return view('table');
});


Route::get('/data_tables', function () {
    return view('data-table');
});

Route::get('/casts/create','CastController@create');
Route::post('/casts','CastController@store');

Route::get('/casts','CastController@index');

Route::get('/casts/{cast_id}','CastController@show');

Route::get('/casts/{cast_id}/edit','CastController@edit');
Route::put('/casts/{cast_id}','CastController@update');

Route::delete('/casts/{cast_id}','CastController@destroy');
