<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('casts.create');
    }

    public function store(Request $request){
        //dd($request->all());
        //dd($request->input('nama'));
        $validateData=$request->validate([
            "nama"=>'required|unique:cast', //unique untuk nama unik
            "umur"=>'required',
            "bio"=>'required'
        ]);


        $query = DB::table('cast')->insert([
            "nama"=>$request["nama"],
            "umur"=>$request["umur"],
            "bio"=>$request["bio"]
        ]);
        return redirect('/casts')->with('success','Cast berhasil disimpan');
    }

    public function index(){
        $cast = DB::table('cast')->get(); //sQL = select * cast
        //dd($cast);
        return view('casts.index',compact('cast'));
    }

    public function show($cast_id){
        $cast=DB::table('cast')->where('id',$cast_id)->first();//menampilkan 1 menggunakan first
        //dd($cast);
        return view('casts.show', compact('cast'));
    }

    public function edit($cast_id){
        $cast=DB::table('cast')->where('id',$cast_id)->first();//menampilkan 1 menggunakan first
        //dd($cast);
        return view('casts.edit', compact('cast'));
    }

    public function update($cast_id, Request $request){

        $validateData=$request->validate([
            "nama"=>'required|unique:cast', //unique untuk nama unik
            "umur"=>'required',
            "bio"=>'required'
        ]);

        $query=DB::table('cast')
                    ->where('id',$cast_id)
                    ->update([
                        'nama'=>$request["nama"],
                        'umur'=>$request["umur"],
                        'bio'=>$request["bio"]
                    ]);
        return redirect('/casts')->with('success','berhasil diubah');
    }

    public function destroy($cast_id){
        $query=DB::table('cast')->where('id',$cast_id)->delete();
        return redirect('/casts')->with('success','berhasil dihapus');
    }
}
